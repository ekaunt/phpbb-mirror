import puppeteer from "puppeteer-core";
import { argv } from "process";
import cheerio from "cheerio";
import axios from "axios";
import fs from "fs";

const BASE_URL = argv[2];
// www.ivelt.com -> ivelt
const HOSTNAME = new URL(BASE_URL).host.split(".").at(-2);
const LOGIN_URL = BASE_URL + "/ucp.php?mode=login";
const USERNAME = argv[3];
const PASSWORD = argv[4];

const LOGFILE = `${HOSTNAME}-history.log`;
const QUEUEFILE = `${HOSTNAME}-queue.log`;
const DELAY = 1; // in s

const downloadedUrls = new Set<string>([]);
const queue = new Array<string>(BASE_URL);

function loadLog() {
  if (!fs.existsSync(LOGFILE)) {
    fs.writeFileSync(LOGFILE, "");
  }
  if (!fs.existsSync(QUEUEFILE)) {
    fs.writeFileSync(QUEUEFILE, "");
  }
  const file = fs.readFileSync(LOGFILE, "utf8");
  file.split("\n").forEach((line) => {
    if (line) {
      downloadedUrls.add(line);
    }
  });
  const queuefile = fs.readFileSync(QUEUEFILE, "utf8");
  queuefile.split("\n").forEach((line) => {
    if (line) {
      queue.push(line);
    }
  });
}

function saveLog() {
  fs.writeFileSync(LOGFILE, Array.from(downloadedUrls).join("\n"));
  fs.writeFileSync(QUEUEFILE, queue.join("\n"));
}

function updateLog(url: string) {
  fs.appendFile(LOGFILE, "\n" + url, () => {});
}

async function mirror() {
  try {
    while (queue.length > 0) {
      const url = queue.pop();
      console.log(url);
      const response = await axios.get(url);
      if (
        response.status === 200 &&
        response.headers["content-type"].includes("text/html")
      ) {
        // Save the HTML file.
        const fullUrl = new URL(url);
        if (fullUrl.pathname.split("/").at(-1).match(/\./) || fullUrl.search) {
          fs.writeFileSync(
            fullUrl.hostname + fullUrl.pathname + fullUrl.search,
            response.data
          );
        } else {
          if (!fs.existsSync(fullUrl.hostname + fullUrl.pathname)) {
            fs.mkdirSync(fullUrl.hostname + fullUrl.pathname, {
              recursive: true,
            });
          }
        }

        // Parse the HTML file.
        const $ = cheerio.load(response.data);

        // Save the HTML file or process it as needed.
        const a = $("a");
        for (let i = 0; i < a.length; ++i) {
          const href = $(a[i]).attr("href");
          if (href) {
            let fullUrl = new URL(href, url).href;
            // remove unwanted url parameters
            fullUrl = fullUrl.replace(/&sid=[a-z0-9]+|sid=[a-z0-9]+&?/, "");
            fullUrl = fullUrl.replace(/&hilit=[^&]*|hilit=[^&]*&?/, "");
            fullUrl = fullUrl.replace(/#.*$/, "");
            fullUrl = fullUrl.replace(/\?$/, "");
            if (downloadedUrls.has(fullUrl)) {
              continue;
            }
            if (
              // skip URLs that don't need to be downloaded
              !fullUrl.startsWith(BASE_URL) ||
              fullUrl.match(
                /bookmark|memberlist.php|faq.php|viewtopic.php.*f=|viewtopic.php.*p=|posting.php|search.php|ucp.php|viewonline.php|view=print|start=0|download|postlove/
              ) ||
              fullUrl.match(
                /\.jpg|\.png|\.gif|\.jpeg|\.pdf|\.mp3|\.mp4|\.avi|\.mkv|\.zip|\.rar|\.7z|\.tar|\.gz|\.bz2|\.xz|\.doc|\.docx|\.xls|\.xlsx|\.ppt|\.pptx|\.odt|\.ods|\.odp|\.txt|\.rtf|\.epub|\.mobi|\.djvu|\.fb2|\.apk|\.exe|\.msi|\.dmg|\.iso|\.torrent|\.js/
              )
            ) {
              continue;
            }

            downloadedUrls.add(fullUrl);
            queue.push(fullUrl);
            updateLog(fullUrl);
          }
        }
      }
      // a random delay to avoid overloading the server.
      const delay = DELAY * (Math.random() + 0.5) * 1000;
      await new Promise((resolve) => setTimeout(resolve, delay));
    }
  } catch (e) {
    saveLog();
    console.log(e);
    return;
  }
}

async function login() {
  const browser = await puppeteer.launch({
    headless: false,
    executablePath: "/usr/bin/chromium",
    devtools: true,
  });
  const page = await browser.newPage();
  await page.setUserAgent(
    "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/119.0.0.0 Safari/537.36"
  );
  await page.goto(LOGIN_URL, { waitUntil: "networkidle0" });
  await page.type("#username", USERNAME);
  await page.type("#password", PASSWORD);
  await page.click("#autologin");

  await Promise.all([
    page.click("input[name=login]"),
    page.waitForNavigation({ waitUntil: "networkidle0" }),
  ]);
  const cookies = await page.cookies();
  //await browser.close();
  return cookies.map((cookie) => cookie.name + "=" + cookie.value).join("; ");
}

function death() {
  console.log("Caught interrupt signal. Saving Logs");
  saveLog();
  process.exit();
}
process.on("SIGINT", death); // CTRL+C
process.on("SIGQUIT", death); // Keyboard quit
process.on("SIGTERM", death); // `kill` command

async function main() {
  //const cookies = await login();
  //console.log(cookies);
  loadLog();
  while (queue.length > 0) {
    await mirror();
  }
  saveLog();
}

main();
