const axios = require("axios");
const cheerio = require("cheerio");
const fs = require("fs");

const BASE_URL = "http://www.kaveshtiebel.com";
const LOGIN_URL = BASE_URL + "/ucp.php?mode=login";
const USERNAME = "שפיגל־מאכער";
const PASSWORD = "Extended-Turbojet-Disclose4-Presume-Shudder";

async function login() {
	try {
		// Fetch login page first to get any required hidden fields (like CSRF tokens)
		const loginPageResponse = await axios.get(LOGIN_URL);
		const $loginPage = cheerio.load(loginPageResponse.data);
		// Example to get a CSRF token, adjust selector based on actual page structure
		const csrfToken = $loginPage('input[name="form_token"]').val();
		console.log({ csrfToken });
		console.log(loginPageResponse.headers["set-cookie"]);
		const cookies = loginPageResponse.headers["set-cookie"];
		const Cookie = cookies.map((cookie) => cookie.split(";")[0]).join("; ");
		const sidCookie = cookies.find((cookie) => cookie.includes("sid="));
		const sid = sidCookie.split(";")[0].split("=")[1];
		console.log({ sid });

		console.log({ Cookie });
		const response = await axios.post(
			LOGIN_URL,
			{
				username: USERNAME,
				password: PASSWORD,
				autologin: true,
				sid,
				redirect: ["./ucp.php?mode=login&redirect=index.php", "index.php"],
				form_token: csrfToken,
				creation_time: Math.floor(Date.now() / 1000),
				login: "איינשרייבן",
			},
			{
				headers: {
					Host: "www.kaveshtiebel.com",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:120.0) Gecko/20100101 Firefox/120.0",
					Accept:
						"text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8",
					"Accept-Language": "en-US,en;q=0.5",
					"Accept-Encoding": "gzip, deflate, br",
					Referer: "https://www.kaveshtiebel.com/ucp.php?mode=login&redirect=index.php",
					"Content-Type": "application/x-www-form-urlencoded",
					"Content-Length": "400",
					Origin: "https://www.kaveshtiebel.com",
					Connection: "keep-alive",
					Cookie,
					"Upgrade-Insecure-Requests": "1",
					"Sec-Fetch-Dest": "document",
					"Sec-Fetch-Mode": "navigate",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-User": "?1",
				},
				withCredentials: true,
				maxRedirects: 0, // Don't follow redirects automatically
				validateStatus: function (status) {
					return status >= 200 && status < 303; // Accept redirects as valid responses
				},
			}
		);
		console.log(response);
		console.log(response.headers);
		const logincookies = response.headers["set-cookie"];
		console.log(logincookies);

		console.log(
			await axios.get("http://www.kaveshtiebel.com/viewtopic.php?t=100", {
				headers: {
					"Content-Type": "text/html; charset=UTF-8",
					Cookie:
						"phpbb3_64ytqhhh_u=7357; phpbb3_64ytqhhh_k=; phpbb3_64ytqhhh_sid=3d261cce7e20d7e0fec7c5e5705ed001",
					Accept:
						"text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8",
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.5",
					Connection: "keep-alive",
					Host: "www.kaveshtiebel.com",
					"Sec-Fetch-Dest": "document",
					"Sec-Fetch-Mode": "navigate",
					"Sec-Fetch-Site": "cross-site",
					"Upgrade-Insecure-Requests": "1",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:120.0) Gecko/20100101 Firefox/120.0",
				},
			})
		);

		return logincookies;
	} catch (error) {
		console.error("Login failed:", error);
	}
}

async function downloadForumPage(url, cookies) {
	try {
		//const Cookie = cookies.map((cookie) => cookie.split(";")[0]).join("; ");
		//console.log({ Cookie });
		const response = await axios.get(url, {
			headers: {
				Accept:
					"text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8",
				"Accept-Encoding": "gzip, deflate, br",
				"Accept-Language": "en-US,en;q=0.5",
				Connection: "keep-alive",
				Cookie: cookies,
				Host: "www.kaveshtiebel.com",
				"Sec-Fetch-Dest": "document",
				"Sec-Fetch-Mode": "navigate",
				"Sec-Fetch-Site": "cross-site",
				"Upgrade-Insecure-Requests": "1",
				"User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:120.0) Gecko/20100101 Firefox/120.0",
			},
			withCredentials: true,
			maxRedirects: 0, // Don't follow redirects automatically
			validateStatus: function (status) {
				return status >= 200 && status < 303; // Accept redirects as valid responses
			},
		});

		return response.data;
	} catch (error) {
		console.error("Failed to download page:", error);
	}
}

function parseAndSave(data, filename) {
	const $ = cheerio.load(data);
	const content = $("body").text(); // Adjust selector based on actual page structure

	fs.writeFileSync(filename, content);
}

async function main() {
	//const cookies = await login();
	//console.log(cookies);
	//if (!cookies) return;

	const i = 100;
	const forumPageUrl = `${BASE_URL}/viewtopic.php?t=${i}`; // Adjust based on actual URL structure
	const cookies =
		"phpbb3_64ytqhhh_u=7357; phpbb3_64ytqhhh_k=8ka9ijhggu4qsaoc; phpbb3_64ytqhhh_sid=89371d546fd2a3b30dc15dc3b63ecec1";
	const forumPageData = await downloadForumPage(forumPageUrl, cookies);

	if (forumPageData) {
		parseAndSave(forumPageData, `forum-page-${i}.html`);
	}
}

main();
